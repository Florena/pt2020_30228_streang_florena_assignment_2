package Model;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy{
    @Override
    public void add(List<QueueClass> qs, ClientClass client) throws InterruptedException {
        for (QueueClass q: qs) {
            if(q.getSize()==0) {
                q.AddClient(client);
                return;
            }

        }

    }


}
