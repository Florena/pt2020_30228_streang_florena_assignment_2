package Model;

public class ClientClass implements Comparable<ClientClass> {
    private int id;
    private int arrival_time;
    private int service_time;

    public ClientClass(int id, int arrival_time, int service_time) {
        this.id = id;
        this.arrival_time = arrival_time;
        this.service_time = service_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getarrival_time() {
        return arrival_time;
    }

    public void setarrival_time(int arrival_time) {
        this.arrival_time = arrival_time;
    }

    public int getService_time() {
        return service_time;
    }

    public void setService_time(int service_time) {
        this.service_time = service_time;
    }

    public boolean greater(ClientClass c)
    {
        if(this.arrival_time>c.arrival_time)
            return true;
        return false;
    }

    @Override
    public String toString() {


        return "Client "
                + id +
                ", arrivaltime=" + arrival_time +
                ", servicetime=" + service_time +
                '}';
    }

    @Override
    public int compareTo(ClientClass clientClass) {
        return arrival_time-clientClass.arrival_time;
    }
}
