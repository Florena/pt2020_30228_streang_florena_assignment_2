package Model;

import java.util.List;

public class ConcreteStrategyTime implements Strategy{
    @Override
    public void add(List<QueueClass> qs, ClientClass client) throws InterruptedException {
            int value_min=1000000;
            int time=0;
        for (QueueClass q: qs) {
            time=q.getTotal_wait().get();
            if(value_min > time)
                value_min=time;


        }

        for (QueueClass q: qs) {
            if(q.getTotal_wait().intValue()==value_min) {
                q.AddClient(client);
                return;
            }
        }
    }


}
