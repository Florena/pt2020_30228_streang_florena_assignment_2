package Controller;

import Model.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class SimulationManagerClass implements Runnable{

   private int timeLimit;
   private int maxServingTime;
   private int minServingTime;
    private int minArrivalTime;
    private int maxArrivalTime;
   private int Q;
   private int N;
   private List<ClientClass> clients;
   private String out_file;
   private SelectionPolicy selectionPolicy=SelectionPolicy.SHORTEST_TIME;
    private Scheduler scheduler;
    private FileWriter fileWriter;
    
    
   public SimulationManagerClass(String input_file, String out_file){
       this.out_file=out_file;
       try {
           fileWriter=new FileWriter(out_file);
       } catch (IOException e) {
           e.printStackTrace();
       }
       Input in=new Input(input_file);
       clients=new ArrayList<>();
       timeLimit=in.getMaxSimulationTime();
       maxServingTime=in.getMaxServiceTime();
       minArrivalTime=in.getMinArrivalTime();
       maxArrivalTime=in.getMaxArrivalTime();
       minServingTime=in.getMinServiceTime();
       Q=in.getNumberOfQueues();
       N=in.getNumberOfClients();
       generateClients();
       scheduler= new Scheduler(Q,N/Q+1);
   }

    private void generateClients() {

        RandomClientGeneratorClass rand=new RandomClientGeneratorClass();
        for(int i=0;i<N;i++){
            ClientClass client=rand.RandomClientGenerator(i+1,minArrivalTime,maxArrivalTime,minServingTime,maxServingTime);
            if(client!=null)
                clients.add(client);
        }

        Collections.sort(clients);

    }

    public int processClients(ClientClass c,int currentTime){

       if(c.getarrival_time()<=currentTime){
           try {

               scheduler.dispatchClient(c);
               clients.remove(c);
               return 1;
           } catch (InterruptedException e) {
               e.printStackTrace();
           }

       }



       return 0;
    }
    public void writer(String text) throws IOException {
        fileWriter.write(text);

    }



    @Override
    public void run() {
       int currentTime= 0;
       while(currentTime<timeLimit){
           try {
               writer("Time "+ currentTime +"\n");
           } catch (IOException e) {
               e.printStackTrace();
           }

           int iterator=0;
           while(iterator<clients.size()){
               ClientClass c=clients.get(iterator);
               if(processClients(c,currentTime)==1) {
                   iterator--;
               }
               iterator++;

           }

           currentTime++;

           try {
               Thread.sleep(1000);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
           printQ(currentTime);

       }
   }
    public void printQ(int currentTime){
        for (QueueClass q:scheduler.getQueueClassList()) {
            try {
                writer(q.toString(currentTime)+"\n");
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

       public void print() throws IOException {

           writer("Waiting clients: "+"\n");
            for (ClientClass c:clients) {
                writer(c.toString()+"\n");

            }
        }




    public static void main(String[] args) throws IOException {
        SimulationManagerClass simulationManagerClass=new SimulationManagerClass(args[0],args[1]);
        try {
            simulationManagerClass.print();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread thread=new Thread(simulationManagerClass);
        thread.run();
        simulationManagerClass.writer("Average waiting time: "+ simulationManagerClass.scheduler.average(simulationManagerClass.N));
        thread.stop();
        try {
            simulationManagerClass.fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
