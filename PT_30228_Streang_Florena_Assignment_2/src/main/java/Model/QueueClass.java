package Model;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Thread.currentThread;
import static java.lang.Thread.sleep;

public class QueueClass implements Runnable{
    private AtomicInteger id;
    private AtomicInteger total_wait;
    private BlockingQueue<ClientClass> blockingQueue;
    private List<Integer> wait_time_per_queue;
    private boolean running;

    public QueueClass(int size, int id) {
        this.id=new AtomicInteger(id);
        this.total_wait= new AtomicInteger(0);
        this.blockingQueue = new ArrayBlockingQueue<ClientClass>(size);
        wait_time_per_queue=new ArrayList<>();
        wait_time_per_queue= Collections.synchronizedList(wait_time_per_queue);

    }

    public void AddClient(ClientClass client) throws InterruptedException {

        blockingQueue.offer(client);
        total_wait.getAndAdd(client.getService_time());
        wait_time_per_queue.add(total_wait.get());
    }

    public void run() {
        while(running){
            if (!blockingQueue.isEmpty()) {
                ClientClass client = blockingQueue.peek();
                if (client != null) {
                    int serv_time=client.getService_time();
                    while(serv_time>=1) {
                        try {
                            sleep( 1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        client.setService_time(serv_time-1);

                    }
                    total_wait.getAndAdd((-1) * client.getService_time());
                    blockingQueue.remove(client);
                }
            }
            else
            {

                synchronized (this){
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }

        }

    }

     public void stop(){
        synchronized (currentThread()){
            notify();
        }
        running=false;
     }

    public AtomicInteger getTotal_wait() {
        return total_wait;
    }

    public List<Integer> getWait_time_per_queue() {
        return wait_time_per_queue;
    }

    public int getSize(){
        return blockingQueue.size();
    }

    public ClientClass[] getBlockingQueue() {
        int iterator=0;
        ClientClass[] clients=new ClientClass[1005];
        for (ClientClass c:blockingQueue) {
            if(c.getService_time()!=0) {
                clients[iterator] = c;
                iterator++;
            }
        }

        return clients;
    }

    public String toString(int currentTime){
        ClientClass[] clients=getBlockingQueue();
        String string="";
        for (ClientClass c:blockingQueue) {
            if(c.getService_time()+c.getarrival_time()>=currentTime)
                string=string+c+"  ; ";
            else
                blockingQueue.remove(c);

        }
        if(blockingQueue.isEmpty())
            return "Queue " + id+ " is closed";
        else
            return "Queue "+id+" "+string;


    }

    public AtomicInteger getId() {
        return id;
    }


}
