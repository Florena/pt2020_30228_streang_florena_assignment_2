package Model;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<QueueClass> queueClassList;
    private int maxNoQ;
    private int maxClientsQ;
    private Strategy strategy;

    public Scheduler(int maxNoQ,int maxClientsQ){
        this.maxNoQ=maxNoQ;
        this.maxClientsQ=maxClientsQ;
        queueClassList=new ArrayList<>();
        changeStrategy(SelectionPolicy.SHORTEST_TIME);
        for(int i=0;i<maxNoQ;++i){
            QueueClass q=new QueueClass(maxClientsQ, i+1);
            queueClassList.add(q);
            Thread thread=new Thread(q);
            thread.start();
        }
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy=new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy=new ConcreteStrategyTime();
        }
    }

    public  void dispatchClient(ClientClass client) throws InterruptedException {
        strategy.add(queueClassList,client);
    }

    public String average(int nr){
        int total=0;
        for (QueueClass q: queueClassList) {
            total+=q.getTotal_wait().get();

        }
        double avg=((double)total)/nr;
        return String.valueOf(avg);
    }

    public List<QueueClass> getQueueClassList() {
        return queueClassList;
    }
}
