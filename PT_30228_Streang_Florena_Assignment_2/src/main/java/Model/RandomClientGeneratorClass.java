package Model;
import java.util.Random;

public class RandomClientGeneratorClass {

    private static int getRandomNumberInRange(int tmin, int tmax) {

        if (tmin >= tmax) {
            throw new IllegalArgumentException("Minim time greater tham maxim time ");
        }

        Random r = new Random();
        return r.nextInt((tmax - tmin) + 1) + tmin;
    }

    public ClientClass RandomClientGenerator(int nrclienti, int minar,int maxar, int minserv, int maxserv ){
        ClientClass newClient=new ClientClass(nrclienti,getRandomNumberInRange(minar,maxar), getRandomNumberInRange(minserv,maxserv));
        return newClient;
    }


}
